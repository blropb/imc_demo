<?php
/**
 * Created by PhpStorm.
 * User: blropb
 * Date: 19.02.2015
 * Time: 18:08
 */


namespace News\Form;

use Zend\Form\Form;

class NewsForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('news');
        $this->setAttribute('method', 'post');
        $this->setInputFilter(new \News\Form\NewsInputFilter());
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'security',
            'type' => 'Zend\Form\Element\Csrf',
        ));
        $this->add(array(
            'name' => 'title',
            'type' => 'Text',
            'class'=>'form-control',
            'options' => array(
                'min' => 3,
                'max' => 25,
                'label' => 'Title',
            ),
        ));
        $this->add(array(
            'name' => 'introtext',
            'type' => 'Textarea',
            'options' => array(
                'label' => 'IntroText',
            ),
        ));
        $this->add(array(
            'name' => 'fulltext',
            'type' => 'Textarea',
            'options' => array(
                'label' => 'FullText',
            ),
        ));
        $this->add(array(
            'name' => 'public',
            'type' => 'Checkbox',
            'options' => array(
                'label' => 'published',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'submitbutton',
            ),
        ));
    }
}
