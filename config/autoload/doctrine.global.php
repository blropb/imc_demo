<?php
/**
 * Created by PhpStorm.
 * User: blropb
 * Date: 19.02.2015
 * Time: 14:24
 */

return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' =>'Doctrine\DBAL\Driver\PDOPgSql\Driver',
                'params' => array(
                    'host'     => 'localhost',
                    'port'     => '5432',
                    'dbname'   => 'zf2_stud_news',
                )
            )
        ),
    ),
);