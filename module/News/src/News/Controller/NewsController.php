<?php
/**
 * Created by PhpStorm.
 * User: blropb
 * Date: 19.02.2015
 * Time: 18:12
 */

namespace News\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use News\Entity;

class NewsController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }
    public function listAction()
    {
        $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $posts = $objectManager
            ->getRepository('\News\Entity\News')
            ->findBy(array(), array('created' => 'DESC'));
        $posts_array = array();
        foreach ($posts as $post) {
            $posts_array[] = $post->getArrayCopy();
        }
        $view = new ViewModel(array(
            'news_list' => $posts_array,
        ));
        return $view;
    }
    public function detailAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            $this->flashMessenger()->addErrorMessage('News id doesn\'t set');
            return $this->redirect()->toRoute('news');
        }
        $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $post = $objectManager
            ->getRepository('\News\Entity\News')
            ->findOneBy(array('id' => $id));
        if (!$post) {
            $this->flashMessenger()->addErrorMessage(sprintf('News with id %s doesn\'t exists', $id));
            return $this->redirect()->toRoute('news');
        }
        // Render template.
        $view = new ViewModel(array(
            'post' => $post->getArrayCopy(),
        ));
        return $view;
    }
    public function addAction()
    {
        $form = new \News\Form\NewsForm();
        $form->get('submit')->setValue('Add');
        $request = $this->getRequest();

        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
                $news_post = new \News\Entity\News();
                $news_post->exchangeArray($form->getData());
                $news_post->setCreated(new \DateTime());
                $news_post->setUpdate(new \DateTime());
                $objectManager->persist($news_post);
                $objectManager->flush();
                $message = 'News succesfully saved!';
                $this->flashMessenger()->addMessage($message);
                // Redirect to list of news_posts
                return $this->redirect()->toRoute('news', array(
                    'action' => 'list'
                ));
            }
            else {
                $message = 'Error while saving news_post';
                $this->flashMessenger()->addErrorMessage($message);
            }
        }
        return array('form' => $form);
    }
    public function editAction()
    {
        // Check if id set.
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            $this->flashMessenger()->addErrorMessage('News id doesn\'t set');
            return $this->redirect()->toRoute('news');
        }
        // Create form.
        $form = new \News\Form\NewsForm();
        $form->get('submit')->setValue('Save');
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $post = $objectManager
                ->getRepository('\News\Entity\News')
                ->findOneBy(array('id' => $id));
            if (!$post) {
                $this->flashMessenger()->addErrorMessage(sprintf('News with id %s doesn\'t exists', $id));
                return $this->redirect()->toRoute('news');
            }
            // Fill form data.
            $form->bind($post);
            return array('form' => $form, 'id' => $id, 'post' => $post);
        }
        else {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
                $data = $form->getData();
                $id = $data['id'];
                try {
                    $news_post = $objectManager->find('\News\Entity\News', $id);
                }
                catch (\Exception $ex) {
                    return $this->redirect()->toRoute('news', array(
                        'action' => 'index'
                    ));
                }
                $news_post->exchangeArray($form->getData());
                $news_post->setUpdate(new \DateTime());
                $objectManager->persist($news_post);
                $objectManager->flush();
                $message = 'News succesfully saved!';
                $this->flashMessenger()->addMessage($message);
                // Redirect to list of News
                return $this->redirect()->toRoute('news', array(
                    'action' => 'list'
                ));
            }
            else {
                $message = 'Error while saving News';
                $this->flashMessenger()->addErrorMessage($message);
                return array('form' => $form, 'id' => $id);
            }
        }
    }
    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            $this->flashMessenger()->addErrorMessage('News id doesn\'t set');
            return $this->redirect()->toRoute('news');
        }
        $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        try {
            $news_post = $objectManager->find('News\Entity\News', $id);
            $objectManager->remove($news_post);
            $objectManager->flush();
        }
        catch (\Exception $ex) {
            $this->flashMessenger()->addErrorMessage('Error while deleting data');
            return $this->redirect()->toRoute('news', array(
                'action' => 'list'
            ));
        }
        $this->flashMessenger()->addMessage(sprintf('News %d was succesfully deleted', $id));
        return $this->redirect()->toRoute('news', array(
            'action' => 'list'
        ));

    }
    public function getTitle(){


    }
}